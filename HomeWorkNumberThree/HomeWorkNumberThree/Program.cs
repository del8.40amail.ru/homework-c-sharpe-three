﻿using System;
using System.Linq;
using static System.Console;
namespace HomeWorkNumberThree
{
    class Program
    {
        static void Main(string[] args)
        {
         
        }
        /// <summary>
        /// Приложение по определению чётного или нечётного числа
        /// </summary>
        static void TaskOne()
        {
            WriteLine("Введите целое число");
            int numberOne = int.Parse(ReadLine());
            WriteLine(numberOne);
            int result = numberOne % 2;
            if (result != 0)
            {
                WriteLine($"Число {numberOne} не является четным");
            }
            else if (result == 0)
            {
                WriteLine($"Число {numberOne} является четным");
            }
            else
            {
                WriteLine("Вы ввели не корректное число, пожалуйста попробуйте снова");
            }
        }

        /// <summary>
        /// Программа подсчёта суммы карт в игре «21»
        /// </summary>
        static void TaskTwo()
        {
            string jack = "J";
            string queen = "Q";
            string king = "K";
            string ace = "A";
            int numberOfCards = 0;
            int totalCardPoints = 0;
            WriteLine("Доброго времени суток, уважаемый игрок. Сколько у вас карт?");
            numberOfCards = int.Parse(ReadLine());
            int[] arrayCards = new int[numberOfCards];
            WriteLine($"Ваше количество карт равно {numberOfCards}");
            WriteLine("Пожалуйста введите цифру исходя из того что вы хотите сделать.");
            WriteLine("1) Узнать стоимость каждой карты.");
            WriteLine("2) Узнать сумму стоимости всех карт.");
            int numberLine = int.Parse(ReadLine());

            switch (numberLine)
            {
                case 1:
                    WriteLine("Карта 2 дает 2 очка");
                    WriteLine("Карта 3 дает 3 очка");
                    WriteLine("Карта 4 дает 4 очка");
                    WriteLine("Карта 5 дает 5 очков");
                    WriteLine("Карта 6 дает 6 очков");
                    WriteLine("Карта 7 дает 7 очков");
                    WriteLine("Карта 8 дает 8 очков");
                    WriteLine("Карта 9 дает 9 очков");
                    WriteLine("Карта 10 дает 10 очков");
                    WriteLine("Карта Валет дает 11 очков и обозначается символом J");
                    WriteLine("Карта Дама дает 12 очков и обозначается символом Q");
                    WriteLine("Карта Король дает 13 очков и обозначается символом K");
                    WriteLine("Карта Туз дает 13 очков и обозначается символом A");
                    break;
                case 2:
                    WriteLine("Для того чтобы узнать сумму всех карт необходимо их перечислить.");
                    for (int i = 0; i < arrayCards.Length; i++)
                    {
                        //временная переменная служащая для замены букв на значения
                        string datePoints = ReadLine();
                        if (datePoints == jack)
                        {
                            WriteLine($"Значение карты под номером {i} равняется {arrayCards[i] = 11}");
                        }
                        if (datePoints == queen)
                        {
                            WriteLine($"Значение карты под номером {i} равняется {arrayCards[i] = 12}");
                        }
                        if (datePoints == king)
                        {
                            WriteLine($"Значение карты под номером {i} равняется {arrayCards[i] = 13}");
                        }
                        if (datePoints == ace)
                        {
                            WriteLine($"Значение карты под номером {i} равняется {arrayCards[i] = 14}");
                        }
                        if (datePoints != jack && datePoints != queen && datePoints != king && datePoints != ace)
                        {
                            WriteLine($"Значение карты под номером {i} равняется {arrayCards[i] = int.Parse(datePoints)}");
                        }
                    }
                    for (int i = 0; i < arrayCards.Length; i++)
                    {
                        totalCardPoints += arrayCards[i];
                    }
                    WriteLine($"Ваше количество очков равно {totalCardPoints}");
                    break;
                default:
                    WriteLine("Что-то пошло не так. Пожалуйста перезапустите приложение и попробуйте снова.");
                    break;
            }
        }

        /// <summary>
        /// Программа для проверки простого числа
        /// </summary>
        static void TaskThree()
        {
            WriteLine("Ввидите целое числе которое нужно проверить на сложность");
            int number = int.Parse(ReadLine());
            int i = 2;
            //Обозначает простое число или нет
            bool primeNumber = true;
            if (number == 1 || number == 0)
            {
                WriteLine($"{number} не является простым числом.");
            }
            while (i < number)
            {
                float result = number % i;
                if (result != 0)
                {
                    primeNumber = false;
                }
                else if (result == 0)
                {
                    primeNumber = true;
                    WriteLine($"{number} не является простым числом.");
                    break;
                }
                i++;
            }
            if (primeNumber == false)
            {
                WriteLine($"{number} является простым числом.");
            }
        }

        /// <summary>
        /// Программа для поиска наименьшего и наибольшего элемент в последовательности
        /// </summary>
        static void TaskFour()
        {
            int sequenceLength = 0;
            int minNumber = 0;
            int maxNumber = 0;
            WriteLine("Доброго времени суток, пользователь, введите длинну последовательности для вычисления наименьшего и наибольшего элемента последовательности.");
            WriteLine($"Длинна вашейл последовательности составляет {sequenceLength = int.Parse(ReadLine())}.");
            string[] arrayStrNumbers = new string[sequenceLength];
            int[] arrayIntNumbers = new int[sequenceLength];
            WriteLine("Введите числа через клавишу Enter (Числа могут быть положительными и отрицательными)");
            for (int i = 0; i < arrayStrNumbers.Length; i++)
            {
                arrayStrNumbers[i] = ReadLine();
                arrayIntNumbers[i] = int.Parse(arrayStrNumbers[i]);
            }
            maxNumber = arrayIntNumbers.Max();
            minNumber = arrayIntNumbers.Min();
            WriteLine($"Максимальным значением строки является {maxNumber}.");
            WriteLine($"Минимальным значением строки является {minNumber}.");
        }

        /// <summary>
        /// Игра «Угадай число»
        /// </summary>
        static void TaskFive()
        {
            int maxNumber = 0;
            WriteLine("Игра угадай число!");
            WriteLine("Ввидите максимальный целочисленный диапазон загадываемого числа.");
            WriteLine($"Диапазон чисел будет начинаться с 0 до {maxNumber = int.Parse(ReadLine())}");
            Random random = new Random();
            int rundomNumber = random.Next(0, maxNumber + 1);
            //Подсчитывает кол-во попыток
            int i;
            for (i = 0; ; i++)
            {
                string userStrNumber = ReadLine();
                if (userStrNumber == "")
                {
                    WriteLine($"Ваше количество попыток равно {i}.");
                    WriteLine($"Загаданным числом являлось {rundomNumber}.");
                    break;
                }
                else
                {
                    int userIntNumber = int.Parse(userStrNumber);
                    if (userIntNumber < rundomNumber)
                    {
                        WriteLine("Ваше число меньше загаданного числа, попробуйте снова.");
                    }
                    if (userIntNumber > rundomNumber)
                    {
                        WriteLine("Ваше число больше загаданного числа, попробуйте снова.");
                    }
                    if (userIntNumber == rundomNumber)
                    {
                        WriteLine("Победа, вы угадали!");
                        WriteLine($"Ваше количество попыток равно {i}.");
                        break;
                    }
                }
            }
        }

    }
}
